require('app-module-path').addPath(__dirname);

const http = require('http');
const logger = require('utils/logger');
const config = require('config/environment');
const db = require('utils/db');
const crons = require('cron/scrap.cron');

crons.scrap.start();

if (config.opbeat.enabled) {
  require('utils/opbeat'); // eslint-disable-line import/no-unassigned-import
}
const express = require('config/express');

const server = http.createServer(express);

(async function () {
  try {
    await db.connectToMongo(config.mongo.uri);
    logger.info('MongoDB [OK]');
    server.listen(config.port, () => {
      logger.info(`Server listening on ${config.port}, in ${config.env} mode`);
    });
  } catch (err) {
    if (err.name === 'MongoNetworkError') {
      logger.error('MongoDB [KO]');
    }
  }
})();
