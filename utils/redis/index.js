const Redis = require('ioredis');
const logger = require('utils/logger');
const {
  redis: redisConf
} = require('config/environment');

class RedisClient {
  constructor(config) {
    this.config = config;
    if (!this.redis && config.enabled) {
      logger.info('Redis [Initializing]');
      this.redis = new Redis(config.connection);

      if (config.renew) {
        this.flushAll();
      }
    }
  }

  removePrefix(keys, separator = ':') {
    return keys.map((key) => key.replace('back-horoscope:', ''));
  }

  async removeAllKeys(keys) {
    if (keys.length === 0) {
      return;
    }

    keys = this.removePrefix(keys);
    const pipeline = this.redis.pipeline();
    keys.forEach((key) => pipeline.del(key));
    await pipeline.exec();
    logger.warn('Redis [Flushing all keys]');
  }

  flushAll() {
    const {
      connection: {
        keyPrefix
      }
    } = this.config;

    const stream = this.redis.scanStream({
      match: `${keyPrefix}*`
    });

    stream.on('data', this.removeAllKeys.bind(this));
  }

  hit(key, result) {
    const { response, hits } = JSON.parse(result);
    this.redis.set(key, JSON.stringify({ response, hits: hits + 1 }));

    return response;
  }

  async fail(key, fn, time) {
    const response = await fn();
    this.redis.set(key, JSON.stringify({ response, hits: 0 }), 'EX', time);

    return response;
  }

  async cacheFn(key, fn, time = 43200) {
    if (!this.config.enabled) {
      logger.info('Redis disabled, avoiding cache');
      return fn();
    }

    const redisResult = await this.redis.get(key);
    if (redisResult) {
      return this.hit(key, redisResult);
    }

    return this.fail(key, fn, time);
  }
}

module.exports = new RedisClient(redisConf);
