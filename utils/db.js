const mongoose = require('mongoose');

function connect(uri) {
  mongoose.connect(uri);
  return mongoose.connection;
}

async function connectToMongo(uri) {
  return new Promise((resolve, reject) => {
    const connection = connect(uri);
    connection.on('error', (err) => {
      reject(err);
    });

    connection.once('open', () => {
      resolve(true);
    });
  });
}

module.exports = {
  connectToMongo
};

