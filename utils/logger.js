const winston = require('winston');
const config = require('config/environment');

const consoleTransport = new winston.transports.Console({
  json: false,
  timestamp: true,
  prettyprint: true,
  colorize: true
});

const logger = new winston.Logger({
  level: config.logLevel,
  transports: [
    consoleTransport
  ]
});

logger.stream = {
  write: function (message, enconding) {
    logger.info(message);
  }
};

module.exports = logger;
