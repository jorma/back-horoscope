const express = require('express');

const router = new express.Router();

// Routers
const apiRouter = require('./api');

// Controllers
const homeController = require('api/controllers/home/home.controller');

router.get('/', homeController.index);
router.get('/robots.txt', homeController.getRobots);

router.use('/api', apiRouter);

module.exports = router;
