const express = require('express');

const router = new express.Router();

const horoscopeController = require('api/controllers/horoscope/horoscope.controller');
const horoscopeValidation = require('api/controllers/horoscope/horoscope.validation');

router.get('/signs/:sign', horoscopeValidation.index, horoscopeController.index);
router.delete('/signs/:_id', horoscopeController.remove);
router.put('/signs/:_id', horoscopeValidation.update, horoscopeController.update);
router.get('/signs', horoscopeValidation.list, horoscopeController.list);
router.post('/signs', horoscopeValidation.create, horoscopeController.create);

module.exports = router;
