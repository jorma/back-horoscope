const express = require('express');
const adminController = require('api/controllers/admin/admin.controller');

const router = new express.Router();

router.get('/', adminController.index);
router.post('/', adminController.login);

module.exports = router;
