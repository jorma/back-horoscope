const HoroscopeRepository = require('api/services/horoscope/horoscope.repository');

async function create(req, res, next) {
  try {
    const { body: { sign, date, description } } = req;
    const projection = {
      sign: 1,
      date: 1,
      description: 1,
      _id: 0
    };

    const horoscope = await HoroscopeRepository.create({ sign, date, description }, projection);
    res.send(horoscope);
  } catch (err) {
    next(err);
  }
}

async function remove(req, res, next) {
  try {
    const { params: { _id } } = req;
    const query = { _id };

    const deleted = await HoroscopeRepository.remove(query);
    if (deleted) {
      return res.status(200).end();
    }

    res.status(404).end();
  } catch (err) {
    next(err);
  }
}

async function update(req, res, next) {
  try {
    const query = req.params;
    const horoscopeUpdated = req.body;

    const horoscope = await HoroscopeRepository.update(query, horoscopeUpdated);
    res.send(horoscope);
  } catch (err) {
    next(err);
  }
}

async function index(req, res, next) {
  try {
    const { params: { sign }, query: { date } } = req;

    const query = { sign, date };
    const projection = {
      sign: 1,
      date: 1,
      description: 1,
      _id: 0
    };

    const horoscope = await HoroscopeRepository.findOne(query, projection);
    if (!horoscope) {
      return res.status(404).end();
    }

    res.status(200).send(horoscope);
  } catch (err) {
    next(err);
  }
}

async function list(req, res, next) {
  try {
    const { query: { page, limit } } = req;
    const projection = {
      sign: 1,
      date: 1,
      description: 1,
      _id: 1
    };

    const opts = {
      projection,
      paginated: {
        page,
        limit
      }
    };

    const { total, list: data } = await HoroscopeRepository.find({}, opts);
    res.send({ data, total });
  } catch (err) {
    next(err);
  }
}

module.exports = { index, create, list, remove, update };
