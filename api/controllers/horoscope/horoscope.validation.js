const { celebrate, Joi } = require('celebrate');
const signEnum = require('utils/enums/signs.enum');

const index = celebrate({
  params: {
    sign: Joi.string().valid(signEnum).required()
  },
  query: {
    date: Joi.date().iso().required()
  }
});

const create = celebrate({
  body: {
    date: Joi.date().iso().required(),
    sign: Joi.string().valid(signEnum).required(),
    description: Joi.string().required()
  }
});

const update = celebrate({
  params: {
    _id: Joi.string().required()
  },
  body: {
    date: Joi.date().iso(),
    sign: Joi.string().valid(signEnum),
    description: Joi.string()
  }
});

const list = celebrate({
  query: {
    page: Joi.number().default(1),
    limit: Joi.number().default(20)
  }
});

module.exports = { index, create, list, update };
