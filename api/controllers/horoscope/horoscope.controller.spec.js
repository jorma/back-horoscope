require('app-module-path/cwd'); // eslint-disable-line import/no-unassigned-import
const test = require('ava');
const request = require('supertest');
const mockery = require('mockery');

mockery.enable({
  useCleanCache: true,
  warnOnUnregistered: false,
  warnOnReplace: false
});

test.beforeEach(() => {
  mockery.resetCache();
});

test.after(() => {
  mockery.deregisterAll();
});

test('GET /:sign (500)', async (t) => {
  const sign = 'sagitario';
  const date = '2018-10-10';
  const HoroscopeRepository = {
    findOne: async (query, projection) => {
      t.deepEqual(query, { sign, date: new Date(date) });
      t.deepEqual(projection, { sign: 1, date: 1, description: 1, _id: 0 });
      throw new Error('TEST_ERROR');
    }
  };
  mockery.registerMock('api/services/horoscope/horoscope.repository', HoroscopeRepository);
  const app = require('config/express');

  await request(app)
    .get(`/api/sign/${sign}?date=${date}`)
    .expect(500);
});

test('GET /:sign (404)', async (t) => {
  const sign = 'sagitario';
  const date = '2018-10-10';

  const HoroscopeRepository = {
    findOne: async (query, projection) => {
      t.deepEqual(query, { sign, date: new Date(date) });
      t.deepEqual(projection, { sign: 1, date: 1, description: 1, _id: 0 });
      return null;
    }
  };

  mockery.registerMock('api/services/horoscope/horoscope.repository', HoroscopeRepository);
  const app = require('config/express');

  await request(app)
    .get(`/api/sign/${sign}?date=${date}`)
    .expect(404);
});

test('GET /:sign (200)', async (t) => {
  const sign = 'sagitario';
  const date = '2018-10-10';
  const expectedResult = {
    sign,
    date: new Date(date).toString(),
    description: 'Lorem ipsum'
  };

  const HoroscopeRepository = {
    findOne: async (query, projection) => {
      t.deepEqual(query, { sign, date: new Date(date) });
      t.deepEqual(projection, { sign: 1, date: 1, description: 1, _id: 0 });
      return expectedResult;
    }
  };

  mockery.registerMock('api/services/horoscope/horoscope.repository', HoroscopeRepository);
  const app = require('config/express');

  const response = await request(app)
    .get(`/api/sign/${sign}?date=${date}`)
    .expect(200);
  t.deepEqual(response.body, expectedResult);
});
