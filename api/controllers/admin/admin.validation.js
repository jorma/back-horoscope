const { celebrate, Joi } = require('celebrate');

const index = celebrate({
  body: {
    username: Joi.string().required(),
    password: Joi.string().required()
  }
});

module.exports = { index };
