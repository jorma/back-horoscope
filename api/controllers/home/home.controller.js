const packageJson = require('package.json');
const RedisClient = require('utils/redis');

function index(req, res, next) {
  try {
    const { name, version } = packageJson;
    res.status(200).send({ name, version });
  } catch (err) {
    next(err);
  }
}

async function getRobots(req, res, next) {
  try {
    res.type('text/plain');
    const rc = await RedisClient.cacheFn('robots', () => {
      return Promise.resolve('User-agent: *\nDisallow: /');
    });

    return res.status(200).send(rc);
  } catch (err) {
    next(err);
  }
}

module.exports = { index, getRobots };
