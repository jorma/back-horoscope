require('app-module-path/cwd'); // eslint-disable-line import/no-unassigned-import
const test = require('ava');
const request = require('supertest');
const mockery = require('mockery');

mockery.enable({
  useCleanCache: true,
  warnOnUnregistered: false,
  warnOnReplace: false
});

test.beforeEach(() => {
  mockery.resetCache();
});

test.after(() => {
  mockery.deregisterAll();
});

test('GET / (200)', async (t) => {
  const app = require('config/express');
  await request(app)
    .get('/')
    .expect(200);
});

test('GET /robots.txt (200)', async (t) => {
  const app = require('config/express');
  const response = await request(app)
    .get('/robots.txt')
    .expect(200);

  t.is(response.header['content-type'], 'text/plain; charset=utf-8');
  t.is(response.header['content-length'], '25');
  t.is(response.header.connection, 'close');
  t.truthy(response.text);
  t.is(response.text, 'User-agent: *\nDisallow: /');
});

test('get / (500)', async (t) => {
  mockery.registerMock('package.json', null);
  const app = require('config/express');

  await request(app)
    .get('/')
    .expect(500);
});

test('get /robots.txt (500)', async (t) => {
  const RedisClient = {
    cacheFn: async () => {
      throw new Error('TEST_ERROR');
    }
  };

  mockery.registerMock('utils/redis', RedisClient);
  const app = require('config/express');

  await request(app)
    .get('/robots.txt')
    .expect(500);
});
