const cheerio = require('cheerio');
const moment = require('moment');
const axios = require('axios');
const RedisClient = require('utils/redis');

class Scrapper {
  _getTomorrowDate($) {
    const scrapped = $('.font12.gray>span').text().replace('Horóscopo del día', '').trim();
    const tomorrow = moment().add(1, 'days').format('DD/MM/YYYY');

    return {
      isTomorrow: scrapped === tomorrow,
      tomorrow
    };
  }

  _getDescription($) {
    return $('span.prediccion').text();
  }

  async fetch(sign) {
    const key = `fetch_${sign}_${moment().add(1, 'days').format('YYYY-MM-DD')}`;
    const rc = await RedisClient.cacheFn(key, async () => {
      const url = `http://www.elhoroscopodehoy.es/horoscopo-manana/${sign}.php`;
      const { data } = await axios.get(url);

      return data;
    });

    return rc;
  }

  scrap(html, sign) {
    const $ = cheerio.load(html);
    const { isTomorrow, tomorrow } = this._getTomorrowDate($);
    if (!isTomorrow) {
      return {};
    }

    const description = this._getDescription($);
    const dateDb = moment(tomorrow, 'DD/MM/YYYY').format('YYYY-MM-DD');

    return {
      sign,
      date: dateDb,
      description
    };
  }
}

module.exports = new Scrapper();
