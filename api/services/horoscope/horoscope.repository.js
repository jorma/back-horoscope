const { Horoscope } = require('api/storage');

class HoroscopeRepository {
  constructor(model) {
    this.model = model;
  }

  async remove(query) {
    const { ok, n } = await this.model.remove(query).exec();
    return (ok === n && ok !== 0);
  }

  async create(data, projection) {
    const horoscope = new Horoscope(data);
    if (!projection) {
      return horoscope.save();
    }

    const { _id } = await horoscope.save();
    return this.findOne({ _id }, projection);
  }

  async update(query, update, opts = {}) {
    const { isNew = false } = opts;
    const params = { new: isNew, select: { _id: 1 } };
    const horoscope = await this.model.findOneAndUpdate(query, { $set: { ...update } }, params);
    return horoscope;
  }

  async find(query, opts) {
    const { projection, paginated: { page, limit } } = opts;
    const cursor = this.model.find(query, projection)
      .skip((page - 1) * limit)
      .limit(limit)
      .sort('-createdAt');
    const promises = [this.model.find(query).count(), cursor.lean().exec()];
    const [total, list] = await Promise.all(promises);
    return { total, list };
  }

  async exists(query) {
    const exists = await this.model.findOne(query).count().lean().exec();
    return Boolean(exists);
  }

  async findOne(query, projection) {
    return this.model.findOne(query, projection).lean().exec();
  }
}

module.exports = new HoroscopeRepository(Horoscope);
