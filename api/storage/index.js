const Horoscope = require('api/storage/models/horoscope');
const User = require('api/storage/models/user');

module.exports = {
  Horoscope,
  User
};
