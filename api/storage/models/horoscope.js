const mongoose = require('mongoose');

const schema = new mongoose.Schema({
  sign: {
    type: String,
    required: true
  },
  date: {
    type: Date,
    required: true,
    index: true
  },
  description: {
    type: String,
    required: true
  }
}, {
  timestamps: true
});

schema.index({ sign: 1, date: 1 });

const Horoscope = mongoose.model('Horoscope', schema);

module.exports = Horoscope;
