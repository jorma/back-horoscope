const { CronJob } = require('cron');
const Scrapper = require('api/services/scrap/scrap.service');
const signs = require('utils/enums/signs.enum');
const config = require('config/environment');
const HoroscopeRepository = require('api/services/horoscope/horoscope.repository');
const logger = require('utils/logger');

const scrap = new CronJob({
  cronTime: config.crons.scrap.frequency,
  onTick: () => {
    logger.info('Initializing CRON');
    signs.forEach(async (sign) => {
      const data = await Scrapper.fetch(sign);
      const toDb = Scrapper.scrap(data, sign);
      const exists = await HoroscopeRepository.exists(toDb);
      if (exists) {
        return;
      }

      await HoroscopeRepository.create(toDb);
    });
  },
  start: false
});

module.exports = {
  scrap
};
