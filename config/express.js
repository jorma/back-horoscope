const express = require('express');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const errorHandler = require('errorhandler');
const celebrate = require('celebrate');
const morgan = require('morgan');
const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const accessControl = require('config/middlewares/access-control');
const config = require('config/environment');
const index = require('api/routes/index');
const logger = require('utils/logger');

const app = express();

app.use(morgan(config.morgan.type, { stream: logger.stream }));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());

app.use(accessControl);

app.use('/', index);

if (config.dashboard.enabled) {
  const session = require('express-session');
  const RedisStore = require('connect-redis')(session);
  app.use(session({
    store: new RedisStore({
      host: config.redis.connection.host,
      port: config.redis.connection.port,
      prefix: 'daily-horoscope:',
      pass: config.redis.connection.password
    }),
    secret: 'daily horoscope',
    resave: false,
    saveUninitialized: true
  }));

  app.use(passport.initialize());
  app.use(passport.session());
  const { User } = require('api/storage');
  passport.use(new LocalStrategy(User.authenticate()));
  passport.serializeUser(User.serializeUser());
  passport.deserializeUser(User.deserializeUser());

  const admin = require('api/routes/admin');
  app.set('view engine', 'pug');
  app.set('views', `${config.root}/api/views`);
  app.use('/static', express.static(`${config.root}/public`));
  app.use('/admin', admin);
}

if (config.opbeat.enabled) {
  const opbeat = require('utils/opbeat');
  logger.info('Opbeat [ ON ]');
  app.use(opbeat.middleware.express());
}

app.use(celebrate.errors());
if (config.env === 'production') {
  const productionHandler = require('config/middlewares/production-handler');
  app.use(productionHandler);
} else {
  app.use(errorHandler({
    log: (error, str) => {
      logger.error(str);
    }
  }));
}

module.exports = app;
