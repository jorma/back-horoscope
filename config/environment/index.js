const path = require('path');
const _ = require('lodash');

// Set default node environment to development
process.env.NODE_ENV = process.env.NODE_ENV || 'development';

const env = process.env.NODE_ENV;
const envConfig = require(`./${env}.js`);

const rootPath = path.normalize(`${__dirname}/../..`);

const all = {
  env,
  logLevel: 'debug',
  morgan: {
    type: 'combined'
  },
  root: rootPath,
  port: process.env.BACK_PORT || 8080,
  opbeat: {
    enabled: false
    // Uncomment if enabled
    // organizationId: '',
    // secretToken: '',
    // appId: ''
  },
  crons: {
    enabled: true,
    scrap: {
      frequency: '*/10 * * * * *'
    }
  },
  mongo: {
    uri: 'mongodb://localhost/back-horoscope'
  },
  redis: {
    enabled: true,
    renew: true,
    connection: {
      host: 'localhost',
      port: 6379,
      password: '',
      db: 0,
      keyPrefix: 'back-horoscope:'
    }
  },
  dashboard: {
    enabled: true
  }
};

const config = _.merge(all, envConfig);
module.exports = config;
