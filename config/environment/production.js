module.exports = {
  crons: {
    scrap: {
      frequency: '*/10 * * * * *'
    }
  },
  redis: {
    connection: {
      host: 'storage_redis',
      password: 'Kores_2016'
    }
  },
  mongo: {
    uri: process.env.MONGO_URI || 'mongodb://localhost/back-horoscope'
  }
};
